const express = require('express')
const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const User = require('../models/User')
const auth = require('../auth')
const Order = require('../models/Order')
const productController = require('./productController')
const Product = require('../models/Product')


module.exports.register = async (body) => {

	const checkEmail = await User.findOne({ email: body.email})
	if (checkEmail) {
		return false
	}

	return User.create({
			firstName: body.firstName,
			lastName: body.lastName,
			email: body.email,
			password: bcrypt.hashSync(body.password, 12),
			isAdmin: body.isAdmin
		})
		.then(result => result)
		.catch(err => err)
}

module.exports.login = async (body) => {
	try {
		const userToLogin = await User.findOne({ email: body.email })
		if (userToLogin) {
			const checkPassword = bcrypt.compareSync(body.password, userToLogin.password)
			if (checkPassword) {
				return { access: auth.createAccessToken(userToLogin) }
			} else {
				return false
			}
		} else {
			return false
		}
	} catch (err) {
		return err
	}
	
}

module.exports.setUserAsAdmin = async (body) => {
	let newAdmin = {
		isAdmin: true
	}
	try {
		const userToUpdateUser = await User.findByIdAndUpdate(body.id, newAdmin)
		const updatedUser = await User.findById(body.id)
		return updatedUser
	} catch(err) {
		return err
	}
	
}

module.exports.checkout = async (id) => {

	const user = await this.getDetails({ userId: id })
	const userCart = user.cart

	let total = 0;
	let orderProducts = []
	for(let product of userCart) {
		const item = await Product.findById(product.productId)
		if (item.isActive) {
			total = total + (item.price * product.quantity);
			orderProducts.push(product)
		}
	}

	const order = new Order({
		userId: id,
		products: orderProducts,
		totalAmount: total
	})

	emptyCart ={
		cart: []
	}

	const userToUpdateUser = await User.findByIdAndUpdate(id, emptyCart)

	return order.save().then(result => result)
		.catch(err => err)
}

module.exports.getDetails = (data) => {
	return User.findById(data.userId)
			.then(result => {
				result.password = ""
				return result
			})
			.catch(err => err)
}

module.exports.getAllOrders = () => {
	return Order.find({})
		.then(result => result)
		.catch(err => err)
}

module.exports.getUserOrders = async (id) => {
	try {
		const userOrders = await Order.find({ userId: id })
		return userOrders
	} catch (err) {
		return err
	}
	
}

module.exports.addProductToCart = async (product, quantity, data) => {
		const user = await this.getDetails({ userId: data.id})
		const productToAdd = await Product.findById(product._id.toString())
		let itemFound = false

		const cartObject = {
			productId: product._id.toString(),
			productName: productToAdd.name,
			productPrice: productToAdd.price,
			quantity: Number(quantity)
		}

		for(let item of user.cart) {
			if (item.productId === cartObject.productId) {
				item.quantity += cartObject.quantity
				itemFound = true 
				break;
			}
		} 

		if (!itemFound) {
			user.cart.push(cartObject)
		}
		
		let updatedCart = {
			cart: user.cart
		}

		const userToUpdateUser = await User.findByIdAndUpdate(data.id, updatedCart)
		return true
	return false
}

