const mongoose = require('mongoose')
const express = require('express')
const Product = require('../models/Product')

module.exports.addProduct = (body) => {
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price,
		isActive: body.isActive
	})

	return newProduct.save().then(result => result)
		.catch(err => err)
}

module.exports.getAllActiveProducts = async () => {
	const activeProducts = await Product.find({ isActive: true })
	return activeProducts;
}

module.exports.getAllProducts = async () => {
	const allProducts = await Product.find()
	return allProducts;
}

module.exports.getProduct = async (id) => {
	const product = await Product.findById(id)
	return product;
}

module.exports.archiveProduct = (id) => {
	return Product.findByIdAndUpdate(id, { isActive: false })	
			.then(result => {
				return result
			}).catch(err => err)
}

module.exports.activateProduct = (id) => {
	return Product.findByIdAndUpdate(id, { isActive: true })	
			.then(result => {
				return result
			}).catch(err => err)
}

module.exports.updateProduct = (id, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price,
		isActive: body.isActive
	}

	return Product.findByIdAndUpdate(id, updatedProduct)
		.then(result => result)
		.catch(err => err)
}

module.exports.deleteProduct = (id) => {
	return Product.findByIdAndDelete(id)
			.then(result => result)
			.catch(err => err)
}
