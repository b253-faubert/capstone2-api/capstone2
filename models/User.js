const mongoose = require('mongoose');
const { Schema } = mongoose

const userSchema = new Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	cart: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productPrice: {
				type: Number,
				required: [true, "Product quantity is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product quantity is required"]
			}
		}
	],
	isAdmin: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model("User", userSchema)