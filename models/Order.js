const mongoose = require('mongoose');
const { Schema } = mongoose

const orderSchema = new Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productPrice: {
				type: Number,
				required: [true, "Product quantity is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Product quantity is required"]
			}
		}
	],
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema)