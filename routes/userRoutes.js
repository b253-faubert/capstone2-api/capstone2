const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const productController = require('../controllers/productController')
const auth = require('../auth')

router.post('/register', (req,res) => {
	userController.register(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
		
})

router.post('/login', (req, res) => {
	userController.login(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.put('/setAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		userController.setUserAsAdmin(req.body)
		.then(result => res.send(result))
		.catch(err => res.send(err))
	} else {
		res.send("You need to be an admin to perform this action")
	}
	
})

router.post('/checkout', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (!userData.isAdmin) {
		userController.checkout(userData.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))
	} else {
		res.send("Please login to a non-admin account to place an order")
	}
	
})

router.get('/userDetails', auth.verify, (req ,res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getDetails({ userId: userData.id })
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.get('/orders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		userController.getAllOrders()
			.then(result => res.send(result))
			.catch(err => res.send(err))
	} else {
		res.send(false)
	}
})

router.get('/myOrders', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (!userData.isAdmin) {
		userController.getUserOrders(userData.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))
	}
	
})

router.put('/addToOrder/:productId', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (!userData.isAdmin) {
		userController.addProductToCart(await productController.getProduct(req.params.productId), req.body.quantity, userData)
		.then(result => res.send(result))
		.catch(err => res.send(err))
	} else {
		res.send(false)
	}
})


module.exports = router