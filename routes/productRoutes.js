const express = require('express')
const router = express.Router()
const auth = require('../auth')
const Product = require('../models/Product')
const productController = require('../controllers/productController')

router.post('/addProduct', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin) {
		productController.addProduct(req.body)
			.then(result => res.send(result))
			.catch(err => res.send(err))
	} else {
		return res.send("You must be an admin to add a product")
	}
})

router.get('/getAllProducts', (req, res) => {
	productController.getAllProducts()
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.get('/', (req, res) => {
	productController.getAllActiveProducts()
		.then(result => res.send(result))
		.catch(err => err)
})

router.get('/:productId', (req, res) => {
	productController.getProduct(req.params.productId)
		.then(result => res.send(result))
		.catch(err => res.send(err))
})

router.put('/:productId/archive', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		try {
			const productToArchive = await productController.archiveProduct(req.params.productId)
			const archivedProduct = await productController.getProduct(req.params.productId)

			return res.send(archivedProduct)
		} catch (err) {
			return res.send(err)
		}
	} else {
		return res.send("You must be an admin to archive a product")
	}
	
})

router.put('/:productId/activate', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		try {
			const productToActivate = await productController.activateProduct(req.params.productId)
			const activatedProduct = await productController.getProduct(req.params.productId)

			return res.send(activatedProduct)
		} catch (err) {
			return res.send(err)
		}
	} else {
		return res.send("You must be an admin to activate a product")
	}
	
})

router.put('/:productId', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		try {
			
			const productToUpdate = await productController.updateProduct(req.params.productId, req.body)
			const updatedProduct = await productController.getProduct(req.params.productId)

			return res.send(updatedProduct)
		} catch (err) {
			return res.send(err)
		}
	} else {
		return res.send(false)
	}
})

router.delete('/:productId', auth.verify, async (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin) {
		try {
			const productToDelete = await productController.deleteProduct(req.params.productId)
			return res.send(true)
		} catch (err) {
			return res.send(err)
		}
	} else {
		return res.send(false)
	}
})	
	

module.exports = router